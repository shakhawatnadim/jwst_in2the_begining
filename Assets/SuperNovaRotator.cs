﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperNovaRotator : MonoBehaviour {

	public GameObject sphere, anotherSphere, center, theRay;
	public float speed, increaseSpeed, movingSpeed;
	private float currentTime;
	private bool turnedOff;

	void Start () {
		
	}
	

	void Update () {

		speed += increaseSpeed;

		if(sphere.transform.localPosition.x<=60){
			turnedOff = true;
			theRay.SetActive (true);
		}

		if(turnedOff){
			currentTime += Time.deltaTime;
		}

		if(currentTime>=5){
			center.SetActive (false);
		}

		if(currentTime>=20){
			gameObject.SetActive (false);
		}

		sphere.transform.localPosition = new Vector3 (sphere.transform.localPosition.x - movingSpeed,
		        sphere.transform.localPosition.y, sphere.transform.localPosition.z);

		anotherSphere.transform.localPosition = new Vector3 (anotherSphere.transform.localPosition.x + movingSpeed,
		        anotherSphere.transform.localPosition.y, anotherSphere.transform.localPosition.z);

		center.transform.RotateAround (center.transform.position, Vector3.down, speed * Time.deltaTime);


	}
}
