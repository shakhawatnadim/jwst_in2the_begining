﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupernovaSwitcher : MonoBehaviour {

	public GameObject [] supernovaList;
	public float waitingTime;


	void Start () {
		StartCoroutine (SwitchSupernova (waitingTime));
	}
	

	void Update () {


	}

	IEnumerator SwitchSupernova(float waitTime){
		for (int i = 0; i < supernovaList.Length; i++) {
			supernovaList[i].SetActive (true);
			yield return new WaitForSeconds (waitTime);
		}
		yield return null;
	}

}
