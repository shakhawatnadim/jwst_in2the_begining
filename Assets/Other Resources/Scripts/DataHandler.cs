﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace mdd.hubble
{
    public class DataHandler : MonoBehaviour
    {
        public Text NameText;
        public GameObject DescriptionPanel;
        public Text descriptionText;
         string objectName;
        public ObjectData[] ObjectDatas;
        private ObjectData Data;
       

        void Start()
        {
          
        }
       

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    objectName = hit.collider.tag;
                   
                    Debug.Log("Object Name"+objectName);
                    
                    if (objectName != null)
                    {
                       GetData(objectName);
                        DescriptionPanel.SetActive(true);
                        string formatted =Data.Description.Replace("\\n", "\n");
                        descriptionText.text = formatted;
                        NameText.text = Data.Name;
                        
                       
                    }
                   
                }
            }

        }

        public void GetData(string name)
        {
           

            foreach (ObjectData data in ObjectDatas)
            {
                if (ObjectDatas!=null)
                {
                    if (data.Name.Equals(name))
                    {
                        Data = data;
                       // Debug.Log("Data name: " + Data.Description);
                    }
                }
                else
                {
                    Debug.LogError("Data is not available");
                }
            }
         
           
        }

        public void OnPressCrossBtn()
        {
            DescriptionPanel.SetActive(false);
        }
    }
}