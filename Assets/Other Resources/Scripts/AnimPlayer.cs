﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimPlayer : MonoBehaviour {
    public AnimationClip clip1;
    public AnimationClip clip2;
    Animation anim;

	// Use this for initialization
	void Start () {
        anim = gameObject.GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Play(string name)
    {
        if (name == "Primary")
        {
            
            anim.clip = clip1;
            Debug.Log("Animation Name: "+anim.clip.name);
        }
        else if (name == "Secondary")
        {
            anim.clip = clip2;
            Debug.Log("Animation Name: " + anim.clip.name);
        }
        else
        {
            Debug.LogError("Name is not assigned");
        }
        anim.Play();
    }
}
