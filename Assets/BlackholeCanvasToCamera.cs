﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackholeCanvasToCamera : MonoBehaviour {


	public GameObject [] blackholes, canvases;
	public GameObject mainCamera;
	


	void Start () {
		

	}
	

	void Update () {
		
		//float lowestDistance = Vector3.Distance (blackholes [0].transform.position, mainCamera.transform.position);

		for (int i = 0; i < canvases.Length; i++) {
			float distance = Vector3.Distance (blackholes [i].transform.position, mainCamera.transform.position);
			canvases [i].SetActive (distance <= 12000);
		}

	}

	void LateUpdate ()
	{
		var target = mainCamera.transform.position;
			
		for (int i = 0; i<canvases.Length;i++)	{
			canvases [i].transform.LookAt (target);
		}
	}

}
