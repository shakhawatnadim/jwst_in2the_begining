﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mdd.jwst
{
    public class Controller : MonoBehaviour
    {

        [SerializeField] GameObject telescope;
        Transform telescopeTransform;
        [SerializeField] GameObject descriptionTextUI;
        [SerializeField] GameObject vCamMain;
        Transform mainCamTransform;
        [SerializeField] PartsViewer partsViewer;
        // Use this for initialization
        void Start()
        {
            telescopeTransform = telescope.transform;
            mainCamTransform = vCamMain.transform;
        }

        public void OnPressFullView()
        {
            partsViewer.isPartialActivated = false;
            telescope.transform.position = telescopeTransform.position;
            telescope.transform.rotation = Quaternion.Euler(0, -90, 0);
            vCamMain.transform.localPosition = mainCamTransform.localPosition;
            vCamMain.transform.rotation = Quaternion.Euler(0, 0, 0);
            descriptionTextUI.SetActive(false);
            telescope.GetComponent<BoxCollider>().enabled = true;
            telescope.GetComponent<RotateObject>().isFullViewActivated = true;
            vCamMain.GetComponent<playermovement>().enabled = true;
        }
        public void OnPressPartsView()
        {
            partsViewer.isPartialActivated = false;
            vCamMain.transform.localPosition = mainCamTransform.localPosition;
            vCamMain.transform.rotation = Quaternion.Euler(0, 0, 0);
            descriptionTextUI.SetActive(true);
            telescope.transform.position = telescopeTransform.position;
            telescope.transform.rotation = Quaternion.Euler(0, -90, 0);
            telescope.GetComponent<RotateObject>().isFullViewActivated = false;
            telescope.GetComponent<BoxCollider>().enabled = false;
            vCamMain.GetComponent<playermovement>().enabled = false;

        }
    }
}