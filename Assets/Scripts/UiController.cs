﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour {

    [SerializeField] GameObject StartText;
    [SerializeField] GameObject mainSpectrumImage;
    [SerializeField] GameObject[] subSpectrums;
    [SerializeField] GameObject subSpectrumParent;
    [SerializeField] Button button;
    [SerializeField] GameObject findText;
	// Use this for initialization
	void Start () {
        button.interactable = false;
        StartCoroutine(SetActiveUI());
	}
	

    IEnumerator SetActiveUI()
    {
        yield return new WaitForSeconds(5);
        StartText.SetActive(false);
        yield return new WaitForSeconds(1);
        mainSpectrumImage.SetActive(true);
        yield return new WaitForSeconds(1);
        subSpectrumParent.SetActive(true);
        yield return new WaitForSeconds(3);
        for (int i = 0; i < subSpectrums.Length; i++)
        {
            subSpectrums[i].SetActive(false);
        }
        button.interactable = true;
        findText.SetActive(true);
    }
   public void OnPressButton()
    {
        findText.SetActive(false);
        subSpectrums[3].SetActive(true);
    }
}
