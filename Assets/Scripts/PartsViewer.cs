﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace mdd.jwst
{
    public class PartsViewer : MonoBehaviour
    {

        float rayDistance;
        public Vector3 targetPos;
        public Vector3 initialPos;
        public float speed;
        float startTime;
        public Text descriptionText;
        public Text nameText;
        public List<ObjectData> targetPositions;
        public List<ObjectData> initialPositions;
        public GameObject mainVcam;
        public GameObject telescope;
        GameObject preSelectedObject;
        public bool isPartialActivated;
        Vector3[] cameraPositions;
        Quaternion[] cameraRotations;
        Transform telescopeTransform;
        Transform mainCamTransform;
        // Use this for initialization
        void Start()
        {
            mainCamTransform = mainVcam.transform;
            telescopeTransform = telescope.transform;
            cameraPositions = new Vector3[targetPositions.Count];
            cameraRotations = new Quaternion[targetPositions.Count];
            isPartialActivated = false;
            startTime = Time.time;
            initialPositions = new List<ObjectData>();
            for (int i = 0; i < telescope.transform.childCount; i++)
            {
                ObjectData data = new ObjectData();
                data.tag = telescope.transform.GetChild(i).gameObject.tag;
                data.position = telescope.transform.GetChild(i).gameObject.transform.localPosition;
                initialPositions.Add(data);

            }

            for (int i = 0; i < targetPositions.Count; i++)
            {

                cameraPositions[i] = targetPositions[i].vCam.transform.localPosition;
                cameraRotations[i] = targetPositions[i].vCam.transform.localRotation;

            }
            //Debug.Log("StartTime==" + startTime);

        }

        // Update is called once per frame
        void Update()
        {
            DetectObject();
            if (Input.GetKeyDown(KeyCode.R))
            {
                ResetTransform();
            }

        }

        public void DetectObject()
        {
            // transform.position = Vector3.Lerp(transform.position, targetPos.position, speed * Time.deltaTime);

            if (!isPartialActivated)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    StopAllCoroutines();

                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                    {

                        if (hit.transform.gameObject != null)
                        {
                            mainVcam.GetComponent<playermovement>().enabled = true;
                            hit.transform.gameObject.GetComponent<RotateObject>().IsRotateOnMousDrag = true;
                            targetPos = FindTargetPosition(hit.transform.gameObject, targetPositions);
                            initialPos = FindTargetPosition(hit.transform.gameObject, initialPositions);
                            // Debug.Log("Parts Name==" + hit.collider.name);
                            // Debug.Log("Target pos==" + targetPos);
                            // Debug.Log("Initial Pos==" + initialPos);
                            ShowText(hit.transform.gameObject);
                            preSelectedObject = hit.transform.gameObject;
                            StartCoroutine(MovePosition(hit.transform.gameObject, initialPos, targetPos));
                            isPartialActivated = true;
                        }

                    }
                }
            }
    

        }

        public Vector3 FindTargetPosition(GameObject selectedObject, List<ObjectData> objectDatas)
        {
            Vector3 selectedPos = Vector3.zero;
            foreach (var item in objectDatas)
            {
                if (selectedObject.tag == item.tag)
                {
                    selectedPos = item.position;

                    break;
                }
            }

            return selectedPos;

        }
        void ShowText(GameObject selectedObject)
        {
            // mainVcam.SetActive(false);
            foreach (var item in targetPositions)
            {
                if (selectedObject.tag == item.tag)
                {
                    descriptionText.text = item.description;
                    nameText.text = item.name;

                    item.vCam.SetActive(true);
                }
                else
                {
                    item.vCam.SetActive(false);
                }
            }
        }

        public void OnPressBack()
        {
            
            StopAllCoroutines();
            mainVcam.SetActive(true);
            mainVcam.GetComponent<playermovement>().enabled = false;
            foreach (var item in targetPositions)
            {
                item.vCam.SetActive(false);
            }
            // StartCoroutine(MovePosition(preSelectedObject, preSelectedObject.transform.position, initialPos));
            ResetTransform();
            isPartialActivated = false;
        }
        void ResetTransform()
        {
            telescope.transform.position = telescopeTransform.position;
            telescope.transform.rotation = Quaternion.Euler(0, -90, 0);
            mainVcam.transform.position = mainCamTransform.position;
            mainVcam.transform.rotation = Quaternion.Euler(0, 0, 0);
            for (int i = 0; i < telescope.transform.childCount; i++)
            {
                telescope.transform.GetChild(i).transform.localPosition = initialPositions[i].position;
                telescope.transform.GetChild(i).transform.localRotation = Quaternion.Euler(0, 0, 0);
                telescope.transform.GetChild(i).gameObject.GetComponent<RotateObject>().IsRotateOnMousDrag = false;
            }
            for (int i = 0; i < targetPositions.Count; i++)
            {
                targetPositions[i].vCam.transform.localPosition = cameraPositions[i];
                targetPositions[i].vCam.transform.localRotation = cameraRotations[i];
            }
        }

        IEnumerator MovePosition(GameObject obj, Vector3 initialPos, Vector3 targetPos)
        {

            Debug.Log(obj.name);
            Debug.Log("Object selected for moving==" + obj.name);
            initialPos = this.initialPos;
            targetPos = this.targetPos;
            Debug.Log("Initial pos==" + initialPos);
            float journeyLength = Mathf.Abs(targetPos.y - initialPos.y);
            startTime = Time.time;
            while (true)
            {
                yield return null;
                float distCovered = (Time.time - startTime) * speed;
                // Debug.Log("Dist covered: " + distCovered);
                float fractionOfJourney = distCovered / journeyLength;
                // Debug.Log("fraction of journey: " + fractionOfJourney);
                obj.transform.localPosition = Vector3.Lerp(initialPos, initialPos + targetPos, fractionOfJourney);
            }
        }
    }
    [System.Serializable]
    public class ObjectData
    {
        public string tag;
        public Vector3 position;
        public GameObject vCam;
        public string description;
        public string name;
    }
}