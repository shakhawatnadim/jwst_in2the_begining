﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfraredViewController : MonoBehaviour {

    public GameObject[] infraredObjects;
    [SerializeField] GameObject targetPosition;
    [SerializeField] float speed;
    [SerializeField] GameObject canvas;

    bool isMoved=false;
   public float maxScale=45;
    // Update is called once per frame
    void Update () {
        MovePosition();
	}


    void MovePosition()
    {
        if (!isMoved)
        {
            if (Vector3.Distance(infraredObjects[0].transform.position, targetPosition.transform.position) != 0)
            {
                infraredObjects[0].transform.position = Vector3.MoveTowards(infraredObjects[0].transform.position, targetPosition.transform.position, speed * Time.deltaTime);
                
            }
            else
            {
                isMoved = true;
                infraredObjects[0].SetActive(false);
                StartCoroutine(Scaler(infraredObjects[1]));
            }
        }
       
        
    }

   
    IEnumerator Scaler(GameObject obj)
    {
        
        while (obj.transform.localScale.z<=maxScale)
        {
            yield return new WaitForSeconds(0.1f);

            obj.transform.localScale += new Vector3(0f,0f,2f);

        }
        yield return null;
        infraredObjects[1].SetActive(false);
        StartCoroutine(Scaler2(infraredObjects[2]));

    }
    IEnumerator Scaler2(GameObject obj)
    {
        maxScale = 3f;
        while (obj.transform.localScale.y <= maxScale)
        {
            yield return new WaitForSeconds(0.1f);

            obj.transform.localScale += new Vector3(0f, 0.2f, 0f);

        }
        yield return new WaitForSeconds(3);
        canvas.SetActive(true);

   

    }
}

