﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mdd.jwst
{
    public class playermovement : MonoBehaviour
    {



        private CharacterController mycc;

        public float movementspeed;

        public GameObject Camera;

        public bool cursorlock;
        public float xaxisclamp;
        public float rotationspeed;

        // Use this for initialization
        void Start()
        {
            xaxisclamp = 0f;
            rotationspeed = 2f;
        }

        // Update is called once per frame
        void Update()
        {

            //MoveCamera();
            //basicmove();
            basicmove();
           // basicrotaion();
        }

        void basicmove()
        {

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                // mycc.Move(transform.right * Time.deltaTime * movementspeed);
                Camera.transform.Translate(transform.right * Time.deltaTime * movementspeed);

            }

            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                // mycc.Move(-transform.right * Time.deltaTime * movementspeed);
                Camera.transform.Translate(-transform.right * Time.deltaTime * movementspeed);

            }

            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                //mycc.Move(transform.forward * Time.deltaTime * movementspeed);
                Camera.transform.Translate(transform.forward * Time.deltaTime * movementspeed);
                // myanimator.SetBool("run", true);
            }


            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                //mycc.Move(-transform.forward * Time.deltaTime * movementspeed);
                Camera.transform.Translate(-transform.forward * Time.deltaTime * movementspeed);

            }


        }

        void MoveCamera()
        {
            float moveX = Input.GetAxis("Horizontal");
            float moveY = Input.GetAxis("Vertical");
            Vector3 rt = new Vector3(-moveX, -moveY, 0);
            Camera.transform.position += rt * Time.deltaTime * movementspeed;
        }
        void basicrotaion()
        {
            float mousex = Input.GetAxis("Mouse X") * Time.deltaTime * rotationspeed;
            float mousey = Input.GetAxis("Mouse Y") * Time.deltaTime * rotationspeed;

            //Debug.Log(mousey);

            //transform.Rotate(new Vector3(0, mousex, 0));
            //xaxisclamp += mousey;
            //if (xaxisclamp > 90f) {
            //    xaxisclamp = 90f;
            //    mousey = 0f;
            //    clampaxisrotationvalue(270f);

            //}
            //if (xaxisclamp < -90f)
            //{
            //    xaxisclamp = -90f;
            //    mousey = 0f;
            //    clampaxisrotationvalue(90f);

            //}

            Camera.transform.Rotate(transform.up * mousex);
            Camera.transform.Rotate(-transform.right * mousey);
        }

        //private void clampaxisrotationvalue(float value)
        //{
        //    Vector3 eulerrotation = transform.eulerAngles;
        //    eulerrotation.x = value;
        //   Camera.transform.eulerAngles = eulerrotation;
        //}
    }

}