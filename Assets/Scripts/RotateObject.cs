﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mdd.jwst
{
    public class RotateObject : MonoBehaviour
    {

        public float speed;
        public float rotSpeed;
        public float keyPressedRotateSpeed;
        float rotX;
        float rotY;
        public bool isFullViewActivated = false;
        Collider collider;
        Vector3 center;
        public bool IsRotateOnMousDrag;

        void Start()
        {
            speed = 1f;
            keyPressedRotateSpeed = 10f;
        }
        void Update()
        {
            if (isFullViewActivated)
            {
                AutoRotator();
            }
            RotateParts();
        }
        void OnMouseDrag()
        {
            if (IsRotateOnMousDrag)
            {
                //collider = GetComponent<MeshCollider>();
                //center = collider.bounds.center;
                rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
                rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;
                transform.Rotate(Vector3.up, -rotX, Space.World);
                transform.Rotate(Vector3.right, rotY, Space.World);
                //transform.RotateAround(center, Vector3.up,-rotX);
                // transform.RotateAround(center, Vector3.right, rotY);
            }


        }


        public void AutoRotator()
        {
            Vector3 rt = new Vector3(1, 0, 0);
            transform.Rotate(rt * Time.deltaTime * speed, Space.Self);
        }

        void RotateParts()
        {
            float moveX = Input.GetAxis("Horizontal");
            float moveY = Input.GetAxis("Vertical");
            Vector3 rt = new Vector3(0, -moveX, -moveY);
            transform.Rotate(rt * Time.deltaTime * keyPressedRotateSpeed, Space.Self);
        }
    }
}