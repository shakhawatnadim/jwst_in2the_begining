﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LaunchingGameScript : MonoBehaviour {

    public GameObject FailPanel, WinPanel;
    public GameObject FullPlane;
    public GameObject smokeTrail1, smokeTrail2, smokeTrail3, smokeTrail4;
    private Transform tr;
    public float speed = 5;
    public GameObject wayPoints;
    public Button pressButton;
    public bool buttonpressed;
    public GameObject destroyParticle;
    public GameObject part1, part2, part3, part4, part5, part6, TrailSmokeObj, NosePairObj;
    public Transform smokePoint1, smokePoint2, smokePoint3, smokePoint4;
    public GameObject[] path;

    public float HeavyBoosterReaseTime = 5f, Part3ReleaseTime = 30f, NosePairEjectTime = 20f;
    public float FinalStageReleaseTime;
    public bool LaunchOK;
    public int currentwayPointId;
    public bool coroutineIBreak, coroutineIIBreak, coroutineIIIBreak, coroutineIVBreak;
    public bool isSpeedStop;
    public float reachDistance = 1.0f;
    public float rotationSpeed = 5.0f;

    public float SpeedFactor = 1.5f;

    public float _time = -5;

    public Text T_cntdwn;

    Vector3 last_position;
    Vector3 current_position;
    public float winTime;

    // Use this for initialization
    void Start()
    {
        coroutineIBreak = false;
        coroutineIIBreak = false;
        coroutineIIIBreak = false;
        coroutineIVBreak = false;
        _time = -5;
        tr = GetComponent<Transform>();
        //StartCoroutine (BreakIt ());
        last_position = transform.position;
        T_cntdwn.text = "T - " + Mathf.Floor(_time);
        pressButton.interactable = false;
        buttonpressed = false;
        LaunchOK = false;
        isSpeedStop = false;
       StartCoroutine(CheckLaunch());

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //T += Time.deltaTime;
        if (isSpeedStop)
        {
           
            
            return;
        }
        _time += Time.deltaTime;

        string ss = " T ";
        if (_time < 0)
            ss += Mathf.Floor(_time);
        else
        {
            ss += " + ";
            ss += Mathf.Floor(_time);
        }
        T_cntdwn.text = ss;
        if (_time < 0)
            return;

        
            
            LaunchGrace();

        if (_time >= HeavyBoosterReaseTime - 1 && !coroutineIBreak)
        {
            coroutineIBreak = true;
            StartCoroutine(BlinkButton());
        }
        if (_time >= HeavyBoosterReaseTime && buttonpressed)
        {
            //pressButton.interactable = true;
            part1.GetComponent<Rigidbody>().isKinematic = false;
            part2.GetComponent<Rigidbody>().isKinematic = false;
            part1.transform.parent = null;
            part2.transform.parent = null;
            smokeTrail1.SetActive(false);
            smokeTrail2.SetActive(false);
        }
        if (_time >= Part3ReleaseTime - 1 && !coroutineIIBreak)
        {
            coroutineIIBreak = true;
            StartCoroutine(BlinkButton());
        }
        if (_time >= Part3ReleaseTime && buttonpressed)
        {
            //Debug.Log (_time);
            part3.GetComponent<Rigidbody>().isKinematic = false;
            TrailSmokeObj.transform.position = smokePoint2.position;
            part3.transform.parent = null;
            smokeTrail3.SetActive(false);
            smokeTrail4.SetActive(true);
        }
        if (_time >= NosePairEjectTime - 1 && !coroutineIIIBreak)
        {
            coroutineIIIBreak = true;
            StartCoroutine(BlinkButton());
        }
        if (_time >= NosePairEjectTime && buttonpressed)
        {
            //Debug.Log (_time);
            NosePairObj.GetComponent<NosePairEjector>().EjectNow = true;
            NosePairObj.transform.parent = null;
        }
        if (_time >= FinalStageReleaseTime - 1 && !coroutineIVBreak)
        {
            coroutineIVBreak = true;
            StartCoroutine(BlinkButton());
        }
        if (_time >= FinalStageReleaseTime && buttonpressed)
        {
            part4.GetComponent<Rigidbody>().isKinematic = false;
            //part.GetComponent<Rigidbody>().isKinematic = false;
            part4.transform.parent = null;
            //part2.transform.parent = null;
            smokeTrail4.SetActive(false);
            //smokeTrail2.SetActive(false);

        }
       /* if(!LaunchOK)
        {
            StopSpeed();
            Debug.Log("DESTROY THE MISSION");
        }*/
        if (_time > 0)
        {
            SpeedFactor += 0.01f * Mathf.Log10(_time);
            //Debug.Log (SpeedFactor + "  " + _time);
            speed += SpeedFactor * Time.deltaTime;
        }

        if (_time > 35)
        {
            BackToMainMenu();
        }
        if(_time>winTime)
        {
            WinStopSpeed();
        }


    }

    void WinStopSpeed()
    {
        isSpeedStop = true;
        WinPanel.SetActive(true);

    }


    private void LaunchGrace()
    {
        //Debug.Log ("Speed = " + speed);
        transform.Translate(speed * transform.right * (-1) * Time.deltaTime);
        ApplySuperCurve();
        ApplyLowCurve();
        //tr.Translate (0, speed * Time.deltaTime, 0);
    }

    void ApplySuperCurve()
    {
        if (10 <= _time && _time <= 25)
        {
            Debug.Log("Super Curving");
            transform.Rotate(new Vector3(0, 1, 0), 2f * Time.deltaTime, Space.Self);
        }
    }

    void ApplyLowCurve()
    {
        if (25 <= _time && _time <= 35)
        {
            transform.Rotate(new Vector3(0, 1, 0), 1 * Time.deltaTime, Space.Self);
        }
    }

    private IEnumerator BreakIt()
    {
        yield return new WaitForSeconds(HeavyBoosterReaseTime);
        part1.GetComponent<Rigidbody>().isKinematic = false;
        part2.GetComponent<Rigidbody>().isKinematic = false;
        part1.transform.parent = null;
        part2.transform.parent = null;

    }


    private void FollowPath()
    {
        float distane = Vector3.Distance(path[currentwayPointId].transform.position, transform.position);

        transform.position = Vector3.MoveTowards(transform.position, path[currentwayPointId].transform.position, speed * Time.deltaTime);

        var rotation = Quaternion.LookRotation(path[currentwayPointId].transform.position - transform.position);
        transform.rotation = (Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationSpeed));
        if (distane <= reachDistance)
        {
            currentwayPointId++;
        }
    }

    public void BackToMainMenu()
    {
        //SceneManager.LoadScene("Menu");
    }

    IEnumerator BlinkButton()
    {
        pressButton.interactable = true;
        yield return new WaitForSeconds(2);
        pressButton.interactable = false;
        buttonpressed = false;
    }

    public void DeactivateParts()
    {
        buttonpressed = true;
    }

    public void StopSpeed()
    {
        isSpeedStop = true;
        destroyParticle.SetActive(true);
        destroyParticle.transform.position = FullPlane.transform.position;
        destroyParticle.transform.rotation = FullPlane.transform.rotation;
        pressButton.interactable = false;
        FailPanel.SetActive(true);
        FullPlane.SetActive(false);

        Debug.Log("Destroy");
    }

    IEnumerator CheckLaunch()
    {
        while (true)
        {
            if(_time>HeavyBoosterReaseTime+1 )
            {
                if (part1.GetComponent<Rigidbody>().isKinematic==true)
                {
                    StopSpeed();
                    break;
                }
            }

            if (_time > Part3ReleaseTime + 1)
            {
                if (part3.GetComponent<Rigidbody>().isKinematic == true)
                {
                    StopSpeed();
                    break;
                }
            }

            if (_time > NosePairEjectTime + 1)
            {
                if (NosePairObj.GetComponent<NosePairEjector>().EjectNow == false)
                {
                    StopSpeed();
                    break;
                }
            }
            if (_time > FinalStageReleaseTime + 1)
            {
                if (part4.GetComponent<Rigidbody>().isKinematic == true)
                {
                    StopSpeed();
                    break;
                }
            }
            yield return null;
        }

       
    }

}