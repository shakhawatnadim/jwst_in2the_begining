﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerlook : MonoBehaviour {

    // Use this for initialization

 
    [SerializeField] private Transform playerbody;
    //private CharacterController mycc;
    //public Animator myanimator;

    //public float movementspeed = 0f;
    public float rotationspeed = 0f;
    private float xaxisclamp;
    void Start () {
     
        xaxisclamp = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        basicrotaion();
    }

    void basicrotaion()
    {
        float mousex = Input.GetAxis("Mouse X") * Time.deltaTime * rotationspeed;
        float mousey = Input.GetAxis("Mouse Y") * Time.deltaTime * rotationspeed;

        //Debug.Log(mousey);

        //transform.Rotate(new Vector3(0, mousex, 0));
        xaxisclamp += mousey;
        if (xaxisclamp > 90f)
        {
            xaxisclamp = 90f;
            mousey = 0f;
            clampaxisrotationvalue(270f);

        }
        if (xaxisclamp < -90f)
        {
            xaxisclamp = -90f;
            mousey = 0f;
            clampaxisrotationvalue(90f);

        }
        transform.Rotate(Vector3.left * mousey);
        playerbody.Rotate(Vector3.up * mousex);
    }

    private void clampaxisrotationvalue(float value)
    {
        Vector3 eulerrotation = transform.eulerAngles;
        eulerrotation.x = value;
        transform.eulerAngles = eulerrotation;
    }
}
