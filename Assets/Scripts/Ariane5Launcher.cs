﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ariane5Launcher : MonoBehaviour {

    public GameObject smokeTrail1, smokeTrail2,smokeTrail3,smokeTrail4;
    private Transform tr;
    public float speed = 5;
    public GameObject wayPoints;
    bool checkCam;
    bool checkfinalcam;
    public TimeManager timeM;
    public bool part12X, part3X, noseX, part4X;
    //public Button pressButton;
    //public bool buttonpressed;

    public GameObject Vcam1, Vcam2, Vcam3;

    public GameObject part1, part2, part3, part4, part5, part6, TrailSmokeObj, NosePairObj;
    public Transform smokePoint1, smokePoint2, smokePoint3, smokePoint4;
    public GameObject[] path;
    public GameObject payLoad1,payLoad2;
    public float HeavyBoosterReaseTime = 5f, Part3ReleaseTime = 30f, NosePairEjectTime = 20f;
    public float FinalStageReleaseTime;

    public int currentwayPointId;

    public float reachDistance = 1.0f;
    public float rotationSpeed = 5.0f;

    public float SpeedFactor = 1.5f;

    public float _time = -5;

    public Text T_cntdwn;

    Vector3 last_position;
    Vector3 current_position;
    private bool isSpeedStop;
    public float winTime;

    // Use this for initialization
    void Start()
    {
        _time = -5;
        tr = GetComponent<Transform>();
        //StartCoroutine (BreakIt ());
        last_position = transform.position;
        T_cntdwn.text = "T - " + Mathf.Floor(_time);
        checkCam = false;
        checkfinalcam = false;
        part12X = false;
        part3X = false;
        part4X = false;
        noseX = false;
        //pressButton.interactable = false;
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //T += Time.deltaTime;
        if (isSpeedStop) return;
        _time += Time.deltaTime;

        string ss = " T ";
        if (_time < 0)
            ss += Mathf.Floor(_time);
        else
        {
            ss += " + ";
            ss += Mathf.Floor(_time);
        }
        T_cntdwn.text = ss;
        if (_time < 0)
            return;


        LaunchGrace();
        
        if (_time >= HeavyBoosterReaseTime && !part12X )
        {
            part12X = true;
            //pressButton.interactable = true;
            part1.GetComponent<Rigidbody>().isKinematic = false;
            part2.GetComponent<Rigidbody>().isKinematic = false;
            part1.transform.parent = null;
            part2.transform.parent = null;
            smokeTrail1.SetActive(false);
            smokeTrail2.SetActive(false);
        }
        if((_time>(HeavyBoosterReaseTime+2) && _time<(NosePairEjectTime-2)) || 
            (_time>(Part3ReleaseTime+2) && _time<(FinalStageReleaseTime -10)) ||
            (_time>(NosePairEjectTime+2) && _time<(Part3ReleaseTime - 2)))
        {
            Debug.Log("entered");
            if(!checkCam)
            {
                checkCam = true;
                Vcam1.SetActive(false);
                Vcam2.SetActive(true);
                Debug.Log("camera2");
            }
        }
        else
        {
            if(!checkfinalcam)
            {
                
                Vcam1.SetActive(true);
                Vcam2.SetActive(false);
                checkCam = false;
               // Debug.Log("camera1");
            }
        }
        
        if (_time >= Part3ReleaseTime && !part3X)
        {
            part3X = true;
            //Debug.Log (_time);
            part3.GetComponent<Rigidbody>().isKinematic = false;
            TrailSmokeObj.transform.position = smokePoint2.position;
            part3.transform.parent = null;
            smokeTrail3.SetActive(false);
            smokeTrail4.SetActive(true);
        }
        
        if (_time >= NosePairEjectTime && !noseX)
        {
            noseX = true;
            //Debug.Log (_time);
            NosePairObj.GetComponent<NosePairEjector>().EjectNow = true;
           // timeM.DoSlowMotion();
            NosePairObj.transform.parent = null;
        }
        
        if (_time >= FinalStageReleaseTime && !part4X)
        {
            part4X = true;
            part4.GetComponent<Rigidbody>().isKinematic = false;
            //part.GetComponent<Rigidbody>().isKinematic = false;
            part4.transform.parent = null;
            //part2.transform.parent = null;
            smokeTrail4.SetActive(false);
            //smokeTrail2.SetActive(false);

        }

        if (_time > 0)
        {
            SpeedFactor += 0.01f * Mathf.Log10(_time);
            //Debug.Log (SpeedFactor + "  " + _time);
            speed += SpeedFactor * Time.deltaTime;
        }

        if (_time > winTime)
        {
            WinStopSpeed();
        }


    }
    void WinStopSpeed()
    {
        isSpeedStop = true;
        payLoad1.SetActive(false);
        payLoad2.SetActive(true);
        checkfinalcam = true;
        Vcam2.SetActive(false);
        Vcam3.SetActive(true);
        // WinPanel.SetActive(true);

    }


    private void LaunchGrace()
    {
        //Debug.Log ("Speed = " + speed);
        transform.Translate(speed * transform.right * (-1) * Time.deltaTime);
        ApplySuperCurve();
        ApplyLowCurve();
        //tr.Translate (0, speed * Time.deltaTime, 0);
    }

    void ApplySuperCurve()
    {
        if (10 <= _time && _time <= 25)
        {
           // Debug.Log("Super Curving");
            transform.Rotate(new Vector3(0, 1, 0), 2f * Time.deltaTime, Space.Self);
        }
    }

    void ApplyLowCurve()
    {
        if (25 <= _time && _time <= 35)
        {
            transform.Rotate(new Vector3(0, 1, 0), 1 * Time.deltaTime, Space.Self);
        }
    }

    private IEnumerator BreakIt()
    {
        yield return new WaitForSeconds(HeavyBoosterReaseTime);
        part1.GetComponent<Rigidbody>().isKinematic = false;
        part2.GetComponent<Rigidbody>().isKinematic = false;
        part1.transform.parent = null;
        part2.transform.parent = null;

    }


    private void FollowPath()
    {
        float distane = Vector3.Distance(path[currentwayPointId].transform.position, transform.position);

        transform.position = Vector3.MoveTowards(transform.position, path[currentwayPointId].transform.position, speed * Time.deltaTime);

        var rotation = Quaternion.LookRotation(path[currentwayPointId].transform.position - transform.position);
        transform.rotation = (Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationSpeed));
        if (distane <= reachDistance)
        {
            currentwayPointId++;
        }
    }

    public void BackToMainMenu()
    {
        //SceneManager.LoadScene("Menu");
    }

   


}
