                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:3195484
Ariane 5 rocket by Vagamar is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

1:86.5 scale model of the ariane 5 rocket

Cura Print settings:

Part 1:
.4 mm line width
layer height .3mm
3 top layers
0 bottom layers
wall line count 3
infill 10% with 2 gradual infill steps
outer wall speed 30
inner wall speed 50
top/bottom speed 40
fan 70%
suport angle 70º

Part 2:
.4 mm line width
layer height .3mm
3 top layers
0 bottom layers
wall line count 3
No infill
outer wall speed 30
inner wall speed 50
top/bottom speed 40
fan 70%
no suport

Part 3:
.4 mm line width
layer height .3mm
3 top layers
0 bottom layers
wall line count 3
no infill
outer wall speed 30
inner wall speed 50
top/bottom speed 40
fan 70%
no supports

Booster top:
.4 mm line width
layer height .3mm
3 top layers
0 bottom layers
wall line count 3
no infill
outer wall speed 30
inner wall speed 50
top/bottom speed 40
fan 70%
no support

Booster Bottom:
.4 mm line width
layer height .3mm
3 top layers
0 bottom layers
wall line count 3
infill 10% with 2 gradual infill steps
outer wall speed 30
inner wall speed 50
top/bottom speed 40
fan 70%
suport angle 70º


Booster motor:
.4 mm line width
layer height .3mm
3 top layers
0 bottom layers
wall line count 3
infill 10% with 2 gradual infill steps
outer wall speed 30
inner wall speed 50
top/bottom speed 40
fan 70%
suport angle 65º


ariane motor:
.4 mm line width
layer height .3mm
3 top layers
0 bottom layers
wall line count 3
infill 10% with 2 gradual infill steps
outer wall speed 30
inner wall speed 50
top/bottom speed 40
fan 70%
suport angle 65º

Booster tube:

No support, top and bottom layers, 100% infill or 6 wall layers to make it solid.

Glue everithing together, prime paint o whatever you want to do.

For the graphics, I printed in clear Inkjet Waterslide Decal Paper, and a layer of clear coat on top of the ink.







# Print Settings

Printer: Anet A8
Resolution: .3
Filament_brand: geeetech
Filament_material: PLA