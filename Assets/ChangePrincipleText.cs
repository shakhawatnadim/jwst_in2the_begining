﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePrincipleText : MonoBehaviour {


	public Text principleText;
	public GameObject panel;


	void Start () {
		StartCoroutine (ChangeRoutineText ());
		StartCoroutine (ShowSeconds ());
	}
	

	void Update () {
		
	}

	IEnumerator ChangeRoutineText(){
		yield return new WaitForSeconds (5);
		string mainText = "There are billions of objects in the universe";
		panel.SetActive (true);
		for (int i = 0; i < mainText.Length; i++) {
			principleText.text = mainText.Substring (0, i+1);
			yield return new WaitForSeconds (0.01f);
		}
		yield return new WaitForSeconds (4f);
		principleText.text = "";
		panel.SetActive (false);
		yield return new WaitForSeconds (6f);
		mainText = "Rays from those far places reach hubble after million or sometimes billions of years";
		panel.SetActive (true);
		for (int i = 0; i < mainText.Length; i++) {
			principleText.text = mainText.Substring (0, i + 1);
			yield return new WaitForSeconds (0.01f);
		}
		yield return new WaitForSeconds (4f);
		principleText.text = "";
		panel.SetActive (false);
		yield return new WaitForSeconds (2f);
		mainText = "Hubble sends the data to a tracking and data relay satellite";
		panel.SetActive (true);
		for (int i = 0; i < mainText.Length; i++) {
			principleText.text = mainText.Substring (0, i + 1);
			yield return new WaitForSeconds (0.01f);
		}
		yield return new WaitForSeconds (5f);
		principleText.text = "";
		panel.SetActive (false);
		yield return new WaitForSeconds (2f);
		mainText = "The relay satellite then sends the data to the White Sands, a ground station location in New Mexico, USA";
		panel.SetActive (true);
		for (int i = 0; i < mainText.Length; i++) {
			principleText.text = mainText.Substring (0, i + 1);
			yield return new WaitForSeconds (0.01f);
		}
		yield return new WaitForSeconds (6f);
		principleText.text = "";
		panel.SetActive (false);
		yield return new WaitForSeconds (7f);
		mainText = "It sends data to Goddard Space Flight Center, Greenbelt, Maryland, USA";
		panel.SetActive (true);
		for (int i = 0; i < mainText.Length; i++) {
			principleText.text = mainText.Substring (0, i + 1);
			yield return new WaitForSeconds (0.01f);
		}
		yield return new WaitForSeconds (4f);
		principleText.text = "";
		mainText = "The Goddard Space Flight Center then sends it to Space Telescope Science Institute, Baltimore, Maryland, USA";
		for (int i = 0; i < mainText.Length; i++) {
			principleText.text = mainText.Substring (0, i + 1);
			yield return new WaitForSeconds (0.01f);
		}
		yield return new WaitForSeconds (5f);
		principleText.text = "";
		mainText = "They perform processing in the last stage which gives the final output";
		for (int i = 0; i < mainText.Length; i++) {
			principleText.text = mainText.Substring (0, i + 1);
			yield return new WaitForSeconds (0.01f);
		}
		yield return new WaitForSeconds (5f);
		panel.SetActive (false);


		yield return null;
	}

	IEnumerator ShowSeconds(){
		int i = 0;

		while(true){
			//Debug.Log (i);
			i++;
			yield return new WaitForSeconds (1f);
		}
	}

}
