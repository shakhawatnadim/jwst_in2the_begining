﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour {

	public AudioSource supernovaAudio;

	void Start () {
		supernovaAudio = GetComponent<AudioSource> ();
	}
	

	void Update () {

		supernovaAudio.Play();

	}
}
